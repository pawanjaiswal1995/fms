import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './store'
import { setFlightSearchDetail } from './actions/flightActions'
import { setCurrentUser } from './actions/authActions'

import Header from './common/Header'
import Landing from './component/layout/Landing'
import Login from './component/auth/Login'
import Service from './component/layout/Service'
import './App.css';
import Flight from './component/layout/Flight';
import Booking from './component/layout/Booking';
import { setBookingDetail } from './actions/bookingActions';


// var now = new Date().getTime();
// var setupTime = localStorage.getItem('setupTime');
// if (setupTime == null) {
//     localStorage.setItem('setupTime', now)
// } else {
//     if(now-setupTime > 30*60*1000) {
//         localStorage.clear()
//         localStorage.setItem('setupTime', now);
//     }
// }

//check for token 

if(localStorage.jwtToken || localStorage.flight) {
  const user = localStorage.jwtToken ? JSON.parse(localStorage.jwtToken) : {}
  const detail = localStorage.flight ? JSON.parse(localStorage.flight) : {}
  const form = localStorage.date ? JSON.parse(localStorage.date) : {}
  const booking = localStorage.booking ? JSON.parse(localStorage.booking) : {}
  store.dispatch(setCurrentUser(user));
  store.dispatch(setFlightSearchDetail(detail,form));
  store.dispatch(setBookingDetail(booking));
}

if (window.Cypress) {
  window.store = store;
}
function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Header />
          <Route exact path="/" component={Landing} />
          <div className="container login">
            <Route exact path="/login" component={Login} />
            <Route exact path="/service" component={Service} />
          </div>
          <Route exact path="/flights" component={Flight} />
          <Route exact path="/booking" component={Booking} />
        </div>
      </Router>
    </Provider>
  );
}

// expose store when run in Cypress
if (window.Cypress) {
  window.store = store
}

export default App;
