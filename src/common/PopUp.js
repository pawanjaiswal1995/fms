import React, { Component } from "react";
import "./css/popup.css"

class PopUp extends Component {
    handleClick = () => {
        this.props.toggle();
    };

    render() {
        const date = this.props.date
        const bid = this.props.bid
        return (
            <div className="modal">
                <div className="modal_content">
                    <span className="close" onClick={this.handleClick}>&times;    </span>
                    <div className="card-section u-box pop-sec">
                        <div className="trip-hdr">
                            <div className="u-ib u-v-align-middle trip-hdr-block-lg">
                                <div className="col-hdr">BOOKING ID</div>
                                <div className="col-cntnt">{bid}</div>
                            </div>
                            <div className="u-ib u-v-align-middle trip-hdr-block-lg">
                                <div className="col-hdr">Booked Date</div>
                                <div className="col-cntnt">{date}</div>
                            </div>
                            <div className="u-ib u-v-align-middle trip-hdr-block-md">
                                <div className="col-hdr">BOOKING STATUS</div>
                                <div className="col-cntnt success">Booking Confirmed</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PopUp