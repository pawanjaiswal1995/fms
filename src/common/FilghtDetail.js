import React, { Component } from 'react'
import data from "../resource/image.json"
import { setFlightDetail } from '../actions/flightActions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import moment from 'moment';

class FilghtDetail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            filghtDetail: data,

        };
    }

    onSubmitHandler(){
        this.props.setFlightDetail(this.props.row);
        this.props.history.push('/login?redirect=booking')
    }

    render() {
        const row = this.props.row;
        const id = this.props.key;
        var time = moment.utc(moment.duration(row.duration, "minutes").asMilliseconds()).format("HH[hr] mm[m] ")
        return (
            <div className="clr"  key = {id}>
                <div className="flexCol whiteBg brdrRd5 fltHpyRsltCard brdrRd5 marginT10">
                    <div className="fl width100 flexCol">
                        <div className="fl width100 dF padLR20 padT10">
                            <div className="col-md-8 col-sm-8 col-xs-8 padLR0">
                                <div className="flexCol width100 padT5 ">
                                    <div className="dF width100 alignItemsCenter">
                                        <img src={this.state.filghtDetail[row.company]} className="flightImagesNew" alt="flights"></img>
                                        <span className="ico13 padR10 padL5">{row.company}</span>
                                    </div>
                                    <div className="col-md-12 col-sm-12 col-xs-12 padLR0 padT10">
                                        <div className="col-md-12  col-sm-12 col-xs-12 padLR0">
                                            <div className="fl width100 dF alignItemsEnd">
                                                <div className="col-md-3 col-sm-3 col-xs-3 padL0 fGS0"><span className="db flexCol"><span className="db textOverflow"><span className="ico12">{row.origin}</span><span className="ico11 greyLt padL5">{row.originLocation}</span></span><span className="fb dF alignItemsCenter ico18 padT5 quicks">{row.segment.length >=1 ? row.segment[0].departureTime : row.segment[row.segment.length - 1].departureTime}</span></span></div>
                                                <div className="col-md-4 col-sm-4 padL0 col-xs-4 fGS0" >
                                                    <div className="dF justifyCenter greyLt ico12"><span>-</span><span><span className="padLR10">{row.segment[0].destinationTerminal}</span><span>-</span></span></div>
                                                    <div className="ico15 fb txtCenter quicks padT5">{time}</div>
                                                </div>
                                                <div className="col-md-1 col-sm-1 col-xs-1 fGS0 width11">&nbsp;</div>
                                                <div className="col-md-3 col-sm-3 col-xs-3 fGS0 width29"><span className="db flexCol"><span className="db textOverflow"><span className="ico12">{row.destination}</span><span className="greyLt ico11 padL5" >{row.destinationLocation}</span></span><span className="fb dF alignItemsCenter ico18 padT5 quicks"><span>{row.segment.length >=1 ? row.segment[0].arrivalTime : row.segment[row.segment.length - 1].arrivalTime}</span></span></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={`dF width100 greyLt ico11 padT5 padB10 ${row.segment.length > 1 ? "" : "vh"}`}>Layover - {row.segment[0].destinationLocation} - {row.segment[0].connectionDuration}</div>
                                </div>
                            </div>
                            <div className="col-md-4 col-xs-4 col-sm-4 padLR0 justifyEnd dF ">
                                <div className="col-md-7 padL0 padR10 justifyBetween flexCol price">
                                    <div className="alignItemsEnd flexCol vh">
                                        <div className="alignItemsCenter dF hpyOffr fmtTooltip">
                                            <i className="icon-offer-badge yellow ico20 padR5"></i><i className="icon-rupee ico8"></i><span className="ico12 fb padR3">0</span><i className="icon-arrow-right ico8"></i>
                                            <div className="tip tip_btm white ">
                                                <div className="fb black"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <span className="alignItemsEnd flexCol">
                                        <span className="strike alignItemsCenter greyLt dF padT5 ico12 vh">
                                            <i className="icon-rupee ico10"></i>
                                            <span className="ico14"></span>
                                        </span>
                                        <span className="alignItemsCenter dF padT2">
                                            <i className="icon-rupee ico12 fb"></i>
                                            <span className="ico20 fb quicks">{row.price}</span>
                                        </span>
                                    </span>
                                </div>
                                <span className="dF flexCol justifyEnd padLR0 col-md-5 alignItemsEnd padB20">
                                    <span className="db clearfix">
                                        <input type="button" value="BOOK" className="button fr fltbook fb widthF105 fb quicks" onClick={this.onSubmitHandler.bind(this)}></input>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div>
                            {/* additional details */}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

FilghtDetail.propTypes = {
    setFlightDetail: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.string.isRequired,
    flight: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    auth : state.auth,
    flight: state.flight,
    errors: state.errors
})

export default connect(mapStateToProps,{ setFlightDetail })(withRouter(FilghtDetail))