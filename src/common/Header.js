import React, { Component } from 'react'
import { Link,withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { logoutUser } from '../actions/authActions'
import { removeFlightDetail, removeErrorDetail} from '../actions/flightActions'
import { removeBookingDetail } from '../actions/bookingActions'
//import logo from './../../resource/img/logo.png'

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    
    onLogout(e) {
        e.preventDefault();
        this.props.logoutUser();
        this.props.removeFlightDetail({})
        this.props.history.push("/")
    }

    onRouteChange(e){
        this.props.removeErrorDetail("")
        this.props.removeBookingDetail({})
        this.props.removeFlightDetail({})
    }

    render() {
        const { isAuthenticated } = this.props.auth;
        const { detail } = this.props.flight;
        const authLinks = (
            <li><span onClick={this.onLogout.bind(this)}>Logout</span></li>                       
        );
        const guestLinks = (
            <li><Link to="/login" onClick={this.onRouteChange.bind(this)}>Login</Link></li>                       
        );
        return (
            <header id="header" className={Object.keys(detail).length === 0 ? "" : "header-scrolled"}>
                <div className="container main-menu">
                    <div className="row align-items-center justify-content-between d-flex">
                        <div id="logo">
                            <a href="/">LOGO</a>
                            {/* <img src={logo} alt="" title="" /> */}
                        </div>
                        <nav id="nav-menu-container">
                            <ul className="nav-menu">
                                <li><Link to="/" onClick={this.onRouteChange.bind(this)}>Home</Link></li>
                                <li><Link to="/service" onClick={this.onRouteChange.bind(this)}>Service</Link></li>
                                {isAuthenticated ? authLinks : guestLinks}
                                {/* <li><a href="contact.html">Contact</a></li> */}
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
        )
    }
}

Header.propTypes = {
    removeErrorDetail: PropTypes.func.isRequired,
    removeBookingDetail: PropTypes.func.isRequired,
    removeFlightDetail: PropTypes.func.isRequired,
    logoutUser : PropTypes.func.isRequired,
    auth : PropTypes.object.isRequired,
    flight: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
}
const mapStateToProps = (state) => ({
    auth : state.auth,
    flight: state.flight
})

export default connect(mapStateToProps, { logoutUser,removeFlightDetail,removeBookingDetail,removeErrorDetail }) (withRouter(Header))
