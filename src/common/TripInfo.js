import React, { Component } from 'react'
import data from "../resource/image.json"
import moment from 'moment';

class TripInfo extends Component {

    constructor(props) {
        super(props);

        this.state = {
            TripInfo: data,

        };
    }

    componentDidMount() {
        console.log("urls", this.state.TripInfo)
    }

    render() {
        const flight = this.props.booking.flightDetail;
        const traveller = this.props.booking.travellerDetail;
        var day = moment(this.props.booking.bookedDate).format("DD MMM, ddd");
        var time = moment.utc(moment.duration(flight.duration, "minutes").asMilliseconds()).format("HH[hr] mm[m] ")
        var layover = flight.segment.length > 1 ? moment.utc(moment.duration(flight.segment[0].connectionDuration, "minutes").asMilliseconds()).format("HH[hr] mm[m] ") : ""
        return (
            <div className="trip-info">
                <div className="airline-info">
                    <div className="img-wrapper u-ib u-v-align-top">
                        <img className="" titl="airline-image" alt="" src={this.state.TripInfo[flight.company]} />
                    </div>
                    <div className="u-ib u-v-align-top flight-detail">
                        <div className="flight-seg"><span>FLIGHT</span><span className="u-interpunct"></span><span>Onward</span></div>
                        <div>
                            <div className="flight-number u-ib u-v-align-middle">{`${flight.company} ${flight.flightNo}`}</div>
                            <div className="flight-status u-ib u-v-align-middle success">Completed</div>
                        </div>
                    </div>
                </div>
                <div className="flight-info">
                    <div className="depart">
                        <div className="col-hdr">DEPARTURE</div>
                        <div className="col-cntnt">{flight.origin} - {flight.segment[0].departureTime}</div>
                        <div className="u-font-w-semi-bold">{day}</div>
                        <div className="city-nm">{flight.originLocation}</div>
                        <div className="arpt-nm">{flight.segment[0].originAirport}</div>
                    </div>
                    <div className="duration">
                        <div className="u-ib duration-cntnt">
                            <div className="col-hdr">DURATION</div>
                            <div className="col-cntnt">{time}</div>
                        </div>
                    </div>
                    <div className="arrive">
                        <div className="col-hdr">ARRIVAL</div>
                        <div className="col-cntnt">{flight.destination} - {flight.segment.length > 1 ? flight.segment[1].arrivalTime : flight.segment[0].arrivalTime}</div>
                        <div className="u-font-w-semi-bold">{day}</div>
                        <div className="city-nm">{flight.destinationLocation}</div>
                        <div className="arpt-nm">{flight.segment.length > 1 ? flight.segment[1].destinationAirport : flight.segment[0].destinationAirport}</div>
                    </div>
                </div>
                <div className="psngr-info">
                    <div className="u-ib passengers-name">
                        <div className="col-hdr">PASSENGER</div>
                        <div className="col-cntnt"><div>{traveller["First Name 1"]}</div></div>
                    </div>
                    <div className="u-ib passengers-type">
                        <div className="col-hdr">TYPE</div>
                        <div className="col-cntnt"><div>Adult</div></div>
                    </div>
                    <div className="u-ib passengers-pnr">
                        <div className="pnr">
                            <div className="col-hdr">PNR / Status</div>
                            <div className="col-cntnt"><div className="PNR">{this.props.booking.id}</div></div>
                        </div>
                    </div>
                </div>
                <div className="btm-brdr"></div>
                {flight.segment.length > 1 ? (<div className="layover">
                    <div className="layover-separator-line"></div>
                    <div className="layover-inner">
                        <span className="text-bold">{layover}</span> layover<span>. Change of flight at <span className="text-bold">{flight.segment[0].destinationLocation}</span></span>
                    </div>
                </div>) : "" }
            </div>
        )
    }
}

export default TripInfo