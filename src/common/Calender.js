import React, { Component } from 'react'
import moment from 'moment';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGreaterThan, faLessThan } from '@fortawesome/free-solid-svg-icons'
import { saveFlightSearchDetail } from './../actions/flightActions'

class Calender extends Component {

    constructor(props) {
        super(props);
        this.state = {
            date: "",
            selecteddate: moment(),
        };
    }

    componentDidMount() {
        let crrdate=moment(this.props.flight.form.start).format("YYYY-MM-DD");
		this.setState({date:crrdate})
    }

    renderTable() {
        const tab = [];
        var refDate = this.state.date;
        var format = 'YYYY-MM-DD',
            f_start = moment(refDate, format).subtract(3, 'days'),
            //f_end = moment(refDate, format).add(7, 'days'),
            today = moment().startOf('day'),
            f_aux = '',
            f_aux_format = '',
            day = '',
            week = '',
            //num_week  = 0,
            month = '',
            clase_today = '',
            clase_middleDay = '',
            clase_disabled = '',
            middleDay = this.props.flight.form.start;
        for (let i = 0; i < 7; i++) {
            clase_disabled = '';
            f_aux = moment(f_start).add(i, 'days');
            f_aux_format = f_aux.format(format);

            day = f_aux.format('DD');
            month = f_aux.locale('en').format('MMM').replace('.', '');
            week = f_aux.locale('en').format('ddd');
            //num_week = f_aux.day();

            f_aux_format == today.format(format) ? clase_today = 'today' : clase_today = '';
            f_aux_format == middleDay ? clase_middleDay = 'middleDay' : clase_middleDay = '';
            var now = new Date(); //todays date
            var end = new Date(f_aux_format);
            var disableDays = end - now;
            // console.log(disableDays + ' ' + f_aux_format);
            if (disableDays > 0) {

                //clase_disabled = 'disabledDay';
            }
            tab.push(
                <td key={i} className={`day_cell ${clase_today} ${clase_middleDay} ${clase_disabled}`} data-celldate={f_aux_format} onClick={this.addTdClickEvent.bind(this)}>
                    <span className="month">{week},{day} {month}</span>
                    <span className="day">₹4803</span>
                    {/* <span className="week"></span> */}
                </td>
            );
        }
        return <tr className="rescalendar_day_cells">{tab}</tr>;
    }

    addTdClickEvent(e) {
        var cellDate = e.currentTarget.getAttribute('data-celldate');
        const newSearch = this.props.flight.form;
        newSearch.start = cellDate
        this.props.saveFlightSearchDetail(newSearch);
        this.renderTable();
    }

    yesterday() {
        var refDate = this.state.date,
        f_ref = '';
        f_ref = moment(refDate, 'YYYY-MM-DD').subtract(3, 'days');
        var currDate = f_ref.format('YYYY-MM-DD')
        var date = moment(currDate).format('YYYY-MM-DD')
        var now = moment();
        if (now > currDate) {
            this.setState({ date: refDate })
        } else {
            f_ref = moment(currDate, 'YYYY-MM-DD').subtract(3, 'days');
            currDate = f_ref.format('YYYY-MM-DD')
            if (now >= currDate) {
                this.setState({ date: refDate })
            } else {
                
                this.setState({ date: currDate })
            }
        }
        this.renderTable();
    }

    tomorrow() {
        var refDate = this.state.date,
            f_ref = '';
        f_ref = moment(refDate, 'YYYY-MM-DD').add(1, 'days');
        var currDate = f_ref.format('YYYY-MM-DD')
        this.setState({ date: currDate })
        this.renderTable();

    }

    render() {
        return (
            <div className="rescalendar" id="my_calendar_calSize">
                <div className="rescalendar my_calendar_calSize_wrapper">
                    <div className="rescalendar_controls">
                        {/* <button className="move_to_yesterday" onClick={this.yesterday.bind(this)}>-</button> */}
                        <div className="slider-decorator-0 prev" onClick={this.yesterday.bind(this)}>
                            <div className="owl-prev" ><FontAwesomeIcon icon={faLessThan} /></div>
                        </div>
                        <table className="rescalendar_table">
                            <thead>
                                {this.renderTable()}
                            </thead>
                        </table>
                        <div className="slider-decorator-1 next" onClick={this.tomorrow.bind(this)}>
                            <div className="owl-next"><FontAwesomeIcon icon={faGreaterThan} /></div>
                        </div>
                        {/* <button className="move_to_tomorrow" onClick={this.tomorrow.bind(this)}>-</button> */}
                    </div>
                </div>
            </div>
        )
    }
}

Calender.propTypes = {
    saveFlightSearchDetail: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.string.isRequired,
    flight: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    auth : state.auth,
    flight: state.flight,
    errors: state.errors
})

export default connect(mapStateToProps,{ saveFlightSearchDetail })(Calender)