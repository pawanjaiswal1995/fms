import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';

class TextGroupForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const title = this.props.title;
        const count = this.props.count;
        const traveller = this.props.pass;
        return (
            <div className="_1P8fKx">
                <div className="E-WKgY">
                    <div className="_1fM-O7">
                        <div className="_3lwNK6"><div className="_2aT300">{title} {count}</div></div>
                    </div>
                    <div className="row _1nEw0S marg">
                        <div className="_2aUroD">
                            <div className="_2uWdcR _3J6mkB">
                                <select className="_3092M2 _3J6mkB _3eHb9o _2e0CBT" >
                                    <option value="" disabled="">Title</option>
                                    <option value="MR">Mr</option>
                                    <option value="MS">Ms</option>
                                </select>
                                <span className="_1LBnEa _182fAu"></span>
                            </div>
                        </div>
                        <div className="col-4-12 _1SKKEL">
                            <div className="_28rJlQ Th26Zc">
                                {Object.keys(traveller).length !== 0 ? 
                                ( <input type="text" className="_16qL6K _3terWh _366U7Q" name={`First Name ${count}`} defaultValue={traveller[`First Name ${count}`]} placeholder="First Name"/> ) 
                                :
                                ( <input type="text" className="_16qL6K _3terWh _366U7Q" name={`First Name ${count}`}  placeholder="First Name"/> ) }
                                
                            </div>
                        </div>
                        <div className="col-4-12 _1SKKEL">
                            <div className="_28rJlQ Th26Zc">
                                {Object.keys(traveller).length !== 0 ? 
                                    (<input type="text" className="_16qL6K _3terWh _366U7Q" name={`Last Name ${count}`} defaultValue={traveller.[`Last Name ${count}`]} placeholder="Last Name" />)
                                    :
                                    (<input type="text" className="_16qL6K _3terWh _366U7Q" name={`Last Name ${count}`} placeholder="Last Name" /> )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

TextGroupForm.propTypes = {
    //history: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.string.isRequired,
    flight: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    auth : state.auth,
    flight: state.flight,
    errors: state.errors
})

export default connect(mapStateToProps,{  })(TextGroupForm)
