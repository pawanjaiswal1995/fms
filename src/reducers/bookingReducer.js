import { SET_BOOKING_DETAIL } from '../actions/types'

const initialState = {
    bookingDetail: {}
}

export default function(state = initialState,action) {
    switch (action.type) {
        case SET_BOOKING_DETAIL :
            return {
                ...state,
                bookingDetail: action.payload
            }
        default:
            return state;
    }
}