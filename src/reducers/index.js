import { combineReducers } from 'redux';
import authReducer from './authReducer';
import errorReducer from './errorReducer';
import flightReducer from './flightReducer';
import bookingReducer from './bookingReducer';

export default combineReducers({
    auth: authReducer,
    errors: errorReducer,
    flight: flightReducer,
    booking: bookingReducer
});