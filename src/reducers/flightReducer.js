import { SET_FLIGHT_DETAIL, SET_SEARCH_DETAIL, SET_SEARCHFLIGHT_DETAIL } from '../actions/types'

const initialState = {
    detail: {},
    form:{}
}

export default function(state = initialState,action) {
    switch (action.type) {
        case SET_SEARCHFLIGHT_DETAIL :
            return {
                ...state,
                detail: action.payload,
                form: action.form
            }
        case SET_SEARCH_DETAIL :
            return {
                ...state,
                form: action.form
            }
        case SET_FLIGHT_DETAIL :
            return {
                ...state,
                detail: action.payload
            }
        default:
            return state;
    }
}