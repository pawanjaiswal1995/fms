import axios from 'axios'
import { GET_ERRORS, SET_BOOKING_DETAIL } from './types'

export const saveBookingDetail = (booking) => dispatch => {
    const url = `http://localhost:3001/booking`
    axios.post(url,booking)
        .then(res => {
            if(Object.keys(res.data).length !==0){
                const detail = res.data;
                dispatch(setBookingDetail(detail))
            }
            else{
                dispatch({
                    type : GET_ERRORS,
                    payload : "Booking Detail is not available for ID"
                }) 
            }
        })
        .catch(err => {
            dispatch({
                type : GET_ERRORS,
                payload : "Error Occured while fetching detail",
            })
        })
}

export const getBookingDetail = (booking) => dispatch => {
    const url = `http://localhost:3001/booking?id=${booking.id}`
    axios.get(url)
        .then(res => {
            if(res.data.length>0){
                const detail = res.data[0];
                localStorage.setItem('booking',JSON.stringify(detail))
                dispatch(setBookingDetail(detail))
                dispatch({
                    type : GET_ERRORS,
                    payload : ""
                })
            }
            else{
                dispatch({
                    type : GET_ERRORS,
                    payload : "Booking is not available for the given date"
                }) 
            }
        })
        .catch(err => {
            dispatch({
                type : GET_ERRORS,
                payload : "Error Occured while fetching detail",
            })
        })
}

export const setBookingDetail = (detail) => {
    return {
        type : SET_BOOKING_DETAIL,
        payload : detail
    }
}

export const removeBookingDetail = () => dispatch => {
    localStorage.removeItem('booking')
    dispatch({
        type : SET_BOOKING_DETAIL,
        payload : {}
    })
}