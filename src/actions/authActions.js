import axios from 'axios'
import { GET_ERRORS, SET_CURRENT_USER } from './types'

//  LOGIN
export const loginUser= (userData) => dispatch => {
    axios.get("http://localhost:3001/user?q=" + userData.username)
        .then(res => {
            console.log("response",res.data)
            if(res.data.length>0){
                if(res.data[0].password === userData.password){
                    const token = res.data[0]
                    localStorage.setItem('jwtToken',JSON.stringify(token))
                    dispatch(setCurrentUser(res.data[0]));
                    dispatch({
                        type : GET_ERRORS,
                        payload : ""
                    })
                }
                else{
                    dispatch({
                        type : GET_ERRORS,
                        payload : "Password is wrong"
                    }) 
                }
            }
            else{
                dispatch({
                    type : GET_ERRORS,
                    payload : "User not present"
                }) 
            }
        })
        .catch(err => {
            dispatch({
                type : GET_ERRORS,
                payload : "Error Occured while fetching login detail"
            })
        })
}

export const setCurrentUser = (user) => {
    return {
        type : SET_CURRENT_USER,
        payload : user
    }
}


//Logout
export const logoutUser = () => dispatch => {
    localStorage.removeItem('jwtToken');
    dispatch(setCurrentUser({}))
}