import axios from 'axios'
import { GET_ERRORS, SET_FLIGHT_DETAIL, SET_SEARCHFLIGHT_DETAIL, SET_SEARCH_DETAIL } from './types'

export const saveFlightSearchDetail = (form) => dispatch => {
    const totalPasseenger = form.child === "" ? form.adults : parseInt(form.adults) + parseInt(form.child)
    form["totalPasseenger"] = totalPasseenger;
    const url = `http://localhost:3001/flights?origin=${form.source}&destination=${form.destination}&vacancy_gte=${totalPasseenger}`
    axios.get(url)
        .then(res => {
            if(res.data.length>0){
                const detail = res.data;
                const result = detail.filter(detail => !detail.exemptDate.includes(form.start))
                if(result.length>0){
                    result.sort((a,b) => {return a["price"] - b["price"]})
                    localStorage.setItem('flight',JSON.stringify(result))
                    localStorage.setItem('date',JSON.stringify(form))
                    dispatch(setFlightSearchDetail(result,form))
                    dispatch(setError("")) 
                }
                else{
                    dispatch(setError("Flight is not available for the given date")) 
                }
            }
            else{
                dispatch(setError("Flight is not available for this route")) 
            }
        })
        .catch(err => {
            dispatch(setError("Error Occured while fetching flight detail"))
        })
}

export const setError = (error) => {
    return {
        type : GET_ERRORS,
        payload : error
    }
}

export const removeErrorDetail = (error) => {
    return {
        type : GET_ERRORS,
        payload : error
    }
}

export const setFlightSearchDetail = (detail,form) => {
    return {
        type : SET_SEARCHFLIGHT_DETAIL,
        payload : detail,
        form : form
    }
}

export const setSearchDetail = (form) => {
    localStorage.setItem('date',JSON.stringify(form))
    return {
        type : SET_SEARCH_DETAIL,
        form : form
    }
}

export const setFlightDetail = (detail) => {
    localStorage.setItem('flight',JSON.stringify(detail))
    return {
        type : SET_FLIGHT_DETAIL,
        payload : detail
    }
}

export const removeFlightDetail = () => dispatch => {
    localStorage.removeItem('flight')
    localStorage.removeItem('date')
    dispatch({
        type : SET_SEARCHFLIGHT_DETAIL,
        payload : {},
        date : {}
    })
}