import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { saveFlightSearchDetail } from '../../actions/flightActions'
import ReactSearchBox from 'react-search-box'
import data from "../../resource/location.json"
import MuiAlert from '@material-ui/lab/Alert';
import { Snackbar } from '@material-ui/core';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Landing extends Component {

    constructor(props) {
        super(props)

        this.state = {
            switch_OneWay: true,
            switch_Round: false,
            minDate: new Date().toISOString().split("T")[0],
            maxDate: new Date(Date.now() +(30 * 86400000)).toISOString().split("T")[0],
            location: data,
            source:"",
            destination:"",
            error: {},
            passState: false,
            formSubmitted: false,
            autoHideSnackBarDuration: 4000,
            formUpdateSeverity: 'success',
            formUpdatedMsg: '',
        }
    }

    componentWillReceiveProps(nextProps) {
        if(Object.keys(nextProps.flight.detail).length !== 0){
            this.setState({ userState: false, passState: false,formSubmitted: true,formUpdateSeverity: 'success',
            formUpdatedMsg: 'Parameters updated successfully', });
            this.props.history.push('/flights')
        }
        if(nextProps.errors !== ""){
            this.setState({
                formSubmitted: true,
                formUpdateSeverity: 'error',
                formUpdatedMsg: 'Failed : ' + nextProps.errors,
            })
        }
    }

    handleSnackBarClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.setState({ formSubmitted: false })
    };


    toggleOne(e) {
        e.preventDefault()
        this.setState({
            switch_OneWay : true,
            switch_Round : false
        })
        
    }

    toggleTwo(e) {
        e.preventDefault()
        this.setState({
            switch_OneWay : false,
            switch_Round : true
        })
    }

    onSelectSource(record) {
        this.setState({source:record.key})
    }

    onSelectTarget(record) {
        this.setState({destination:record.key})
    }

    oneWaySubmitHandler = (event) => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const form = {};
        for (let [key, value] of formData.entries()) {
            form[key] = value
        }
        form["source"]=this.state.source;
        form["destination"]=this.state.destination;
        console.log("Form Data", form);
        if(form.source !== "" && form.destination !== "" && form.adults !== ""){
            this.props.saveFlightSearchDetail(form);
        }
        
    }


    render() {
        return (
            <section className="banner-area relative">
                <div className="overlay overlay-bg"></div>
                <div className="container">
                    <div className="row fullscreen align-items-center justify-content-between">
                        <div className="col-lg-6 col-md-6 banner-left">
                            <h6 className="text-white">Away from monotonous life</h6>
                            <h1 className="text-white">Magical Travel</h1>
                            <p className="text-white">
                                If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.
							</p>
                            <a href="/" className="primary-btn text-uppercase">Get Started</a>
                        </div>
                        <div className="col-lg-4 col-md-6 banner-right">
							<ul className="nav nav-tabs" id="myTab" role="tablist">
							  <li className="nav-item">
							    <span className={`nav-link ${this.state.switch_OneWay ? "active" : ""}`} onClick={this.toggleOne.bind(this)}>One-Way Trip</span>
							  </li>
							  <li className="nav-item">
							    <span className={`nav-link ${this.state.switch_Round ? "active" : ""}`} onClick={this.toggleTwo.bind(this)}>Round Trip</span>
							  </li>
							</ul>
							<div className="tab-content" id="myTabContent">
                                <div className={`"tab-pane fade" ${this.state.switch_OneWay ? "show active" : "disable"}`} id="OneWay" >
                                    <form className="form-wrap" onSubmit={this.oneWaySubmitHandler}>
                                        <ReactSearchBox 
                                            placeholder="From "
                                            className= "from"
                                            value=""
                                            data={this.state.location}
                                            onSelect={(record) =>  this.onSelectSource(record)}
                                         />
                                         <ReactSearchBox
                                            placeholder="To "
                                            className= "to"
                                            value=""
                                            data={this.state.location}
                                            onSelect={(record) =>  this.onSelectTarget(record)}
                                         />
                                        <input type="date" className="form-control date-picker" name="start" placeholder="Start " min={this.state.minDate} max={this.state.maxDate} ></input>
                                        <input type="date" className="form-control date-picker" name="return" placeholder="Return "  disabled></input>
                                        <input type="number" min="1" max="20" className="form-control" name="adults" placeholder="Adults " ></input>
                                        <input type="number" min="0" max="20" className="form-control" name="child" placeholder="Child <= 2 year"></input>	
                                        <button className="primary-btn text-uppercase oneWay">Search flights</button>		
                                        
                                    </form>
							    </div>
							    <div className={`"tab-pane fade" ${this.state.switch_Round ? "show active" : "disable"}`} id="Round" >
                                    <form className="form-wrap">
                                        <input type="text" className="form-control" name="name" placeholder="From1 " ></input>								
                                        <input type="text" className="form-control" name="to" placeholder="To1 " ></input>
                                        <input type="date" className="form-control date-picker" name="start1" placeholder="Start1 " min={this.state.minDate} max={this.state.maxDate}></input>
                                        <input type="date" className="form-control date-picker" name="return1" placeholder="Return1 " min={this.state.minDate} max={this.state.maxDate}></input>
                                        <input type="number" min="1" max="20" className="form-control" name="adults1" placeholder="Adults1 " ></input>
                                        <input type="number" min="1" max="20" className="form-control" name="child1" placeholder="Child1 " ></input>				
                                        <button className="primary-btn text-uppercase">Search flights</button>	
                                        {/* <a href className="primary-btn text-uppercase">Search flights</a>									 */}							
                                    </form>							  	
							    </div>
							</div>
						</div>
                    </div>
                </div>
                <Snackbar open={this.state.formSubmitted} autoHideDuration={this.state.autoHideSnackBarDuration} onClose={(event, reason) => this.handleSnackBarClose(event, reason)}>
                    <Alert onClose={(event, reason) => this.handleSnackBarClose(event, reason)} severity={this.state.formUpdateSeverity}>
                        {this.state.formUpdatedMsg}
                    </Alert>
                </Snackbar>
            </section>
        )
    }
}

Landing.propTypes = {
    saveFlightSearchDetail: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.string.isRequired,
    flight: PropTypes.object.isRequired

}

const mapStateToProps = (state) => ({
    auth : state.auth,
    flight: state.flight,
    errors: state.errors
})
export default connect(mapStateToProps,{ saveFlightSearchDetail })(Landing)