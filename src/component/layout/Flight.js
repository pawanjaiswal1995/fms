import React, { Component } from 'react'
import "../css/flight.css";
import moment from 'moment';
import FilghtDetail from '../../common/FilghtDetail'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import Calender from '../../common/Calender';

class Flight extends Component {

    constructor(props) {
        super(props);
        this.state = {
            date: "",
            selecteddate: moment(),
            detail: [],
            detailForm: [],

        };
    }

    componentDidMount() {
        if (Object.keys(this.props.flight.detail).length === 0) {
            this.props.history.push('/')
        }
    }

    componentWillReceiveProps(nextProps) {
        if (Object.keys(nextProps.flight.detail).length === 0) {
            this.props.history.push('/')
        }
    }

    renderFlightTable() {
        if (this.state.detail.length === 0 || JSON.stringify(this.state.detailForm) !== JSON.stringify(this.props.flight.detail)) {
            this.setState({ detail: this.props.flight.detail, detailForm: this.props.flight.detail })
            return this.props.flight.detail.map((row, index) => {
                return (<FilghtDetail row={row} key={row.id} />)
            })
        } else {
            return this.state.detail.map((row, index) => {
                return (<FilghtDetail row={row} key={row.id} />)
            })
        }
    }

    departure(e) {
        var cellDate = e.currentTarget.getAttribute('data-cy');
        const flight = this.props.flight.detail
        let final = []
        if (cellDate === "timeFilter0") {
            final = flight.filter(flit => flit.departureState === "morning");
        } else if (cellDate === "timeFilter1") {
            final = flight.filter(flit => flit.departureState === "noon");
        } else if (cellDate === "timeFilter2") {
            final = flight.filter(flit => flit.departureState === "evening");
        } else {
            final = flight.filter(flit => flit.departureState === "night");
        }
        this.setState({ detail: final })
    }

    splitFlight(e) {
        var cellDate = e.currentTarget.getAttribute('data-cy');
        const flight = this.props.flight.detail
        let final = []
        if (cellDate === "stopsFilter0") {
            final = flight.filter(flit => flit.segment.length === 1);
        } else {
            final = flight.filter(flit => flit.segment.length > 1);
        }
        this.setState({ detail: final })
    }

    resetAll() {
        this.setState({ detail: this.props.flight.detail })
    }


    render() {
        if (Object.keys(this.props.flight.detail).length === 0) {
            return (
                <div>
                    <h1>No Flight Available</h1>
                </div>
            )
        }
        else {
            return (
                <div className="flight-dt">
                    <div className="fltHpyFilter dF flexCol">
                        <div className="fltHpyFilter whiteBg brdRd8" id="filterContainer">
                            <div className="stickyHpy pad20">
                                <div className="dF justifyBetween black padB20 marginB10 quicks">
                                    <span className="fb ico16">Filters</span>
                                    <span className="grey ico12 curPointFlt" onClick={this.resetAll.bind(this)}>Reset All</span>
                                </div>
                                <div className="whiteBg brdrRd5 flexCol marginB5">
                                    <div>
                                        <div className="whiteBg brdrRd5 flexCol marginB5">
                                            <div className="black fb ico14 padB10 quicks">Departure</div>
                                            <div className="dF padT10 justifyBetween fltrStp flexWrap">
                                                <label className="flexCol width50 alignItemsCenter justifyCenter curPointFlt">
                                                    <span data-cy="timeFilter0" className="fltHpyStp paleGreyBg3 ico11 padLR5 padTB10 txtCenter" onClick={this.departure.bind(this)}>4am - 11am</span>
                                                </label>
                                                <label className="flexCol width50 alignItemsCenter justifyCenter curPointFlt">
                                                    <span data-cy="timeFilter1" className="fltHpyStp paleGreyBg3 ico11 padLR5 padTB10 txtCenter" onClick={this.departure.bind(this)}>11am - 4pm</span>
                                                </label>
                                                <label className="flexCol width50 alignItemsCenter justifyCenter curPointFlt">
                                                    <span data-cy="timeFilter2" className="fltHpyStp paleGreyBg3 ico11 padLR5 padTB10 txtCenter" onClick={this.departure.bind(this)}>4pm - 9pm</span>
                                                </label>
                                                <label className="flexCol width50 alignItemsCenter justifyCenter curPointFlt">
                                                    <span data-cy="timeFilter3" className="fltHpyStp paleGreyBg3 ico11 padLR5 padTB10 txtCenter" onClick={this.departure.bind(this)}>9pm - 4am</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="borderBtm flexCol marginNLR20 marginB20"> &nbsp;</div>
                                </div>
                                <div className="whiteBg brdrRd5 flexCol marginB5">
                                    <div className="black fb ico14 padB10 quicks">Stops</div>
                                    <div className="dF padT10 justifyBetween fltrStp flexWrap" data-cy="totalStops4">
                                        <label className="flexCol width50 alignItemsCenter justifyCenter">
                                            <span data-cy="stopsFilter0" className="fltHpyStp paleGreyBg3 ico11 padLR5 padTB10 txtCenter curPointFlt" onClick={this.splitFlight.bind(this)}>
                                                <span className="ico16 fb">0 </span>Stop
                                        </span>
                                        </label>
                                        <label className="flexCol width50 alignItemsCenter justifyCenter">
                                            <span data-cy="stopsFilter1" className="fltHpyStp paleGreyBg3 ico11 padLR5 padTB10 txtCenter curPointFlt" onClick={this.splitFlight.bind(this)}>
                                                <span className="ico16 fb" >1 </span>Stops
                                        </span>
                                        </label>
                                    </div>
                                    <div className="borderBtm flexCol marginNLR20 marginB20"> &nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="fltHpyResults intRnd">
                        <div>
                            <div className="whiteBg brdrRd5 cal" >
                                <div className="fl width100 whiteBg posRel oh marginTB5 ht" id="fareTrendsDiv" >
                                    <Calender />
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="quicks">
                                <div className="fb padTB10 ico14">Sort By</div>
                                <ul className="fltHpySrt ico12 dF justifyAround whiteBg pad10 brdrRd5">
                                    <li id="DEPARTURE" className="curPointFlt">
                                        <span className="dF alignItemsCenter justifyCenter">
                                            <span className="fb ico11 ">
                                                DEPARTURE
                                    </span>
                                        </span>
                                    </li>
                                    <li id="DURATION" className="curPointFlt">
                                        <span className="dF alignItemsCenter justifyCenter">
                                            <span className="fb ico11 ">
                                                DURATION
                                    </span>
                                        </span>
                                    </li>
                                    <li id="ARRIVAL" className="curPointFlt">
                                        <span className="dF alignItemsCenter justifyCenter">
                                            <span className="fb ico11 ">
                                                ARRIVAL
                                    </span>
                                        </span>
                                    </li>
                                    <li id="PRICE" className="curPointFlt">
                                        <span className="dF alignItemsCenter justifyCenter">
                                            <span className="fb ico11  hpyBlueLt ">
                                                PRICE
                                    </span>
                                            <i className="ico13 icon-arrow2-up hpyBlueLt "></i>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <div className="marginB10">
                                {this.renderFlightTable()}
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

Flight.propTypes = {
    auth: PropTypes.object.isRequired,
    errors: PropTypes.string.isRequired,
    flight: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    flight: state.flight,
    errors: state.errors
})

export default connect(mapStateToProps, {})(Flight)