import React, { Component } from 'react'
import TripInfo from '../../common/TripInfo'
import '../css/service.css'
import { getBookingDetail } from '../../actions/bookingActions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import MuiAlert from '@material-ui/lab/Alert';
import { Snackbar } from '@material-ui/core';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Service extends Component {
    constructor(props) {
        super(props);
        this.state = {
            result: false,
            passState: false,
            formSubmitted: false,
            autoHideSnackBarDuration: 4000,
            formUpdateSeverity: 'success',
            formUpdatedMsg: '',
        };
    }

    componentWillReceiveProps(nextProps) {
        if(Object.keys(nextProps.booking.bookingDetail).length !== 0){
            this.setState({ formSubmitted: false,formUpdateSeverity: 'success',
            formUpdatedMsg: '', });
        }
        if(nextProps.errors !== ""){
            this.setState({
                formSubmitted: true,
                formUpdateSeverity: 'error',
                formUpdatedMsg: 'Failed : ' + nextProps.errors,
            })
        }
    }

    handleSnackBarClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.setState({ formSubmitted: false })
    };

    onSubmitHandler(e) {
        e.preventDefault();
        const formData = new FormData(e.target);
        const booking = {};
        for (let [key, value] of formData.entries()) {
            booking[key] = value
        }
        if (booking.id !== "" && booking.name !== "") {
            this.props.getBookingDetail(booking);
            this.setState({ result: true })
        }

    }

    renderBookingDetail() {
        if (Object.keys(this.props.booking.bookingDetail).length === 0) {
            return (
                <div></div>
            )
        }
        else {
            const fareDetail = this.props.booking.bookingDetail.fareDetail
            return (
                <div className={`page-content u-layout-2-col`}>
                    <div className="u-ib u-layout-col-1">
                        <div className="trip-dtl">
                            <div className="card-section u-box">
                                <div className="trip-hdr">
                                    <div className="u-ib u-v-align-middle trip-hdr-block-lg">
                                        <div className="col-hdr">BOOKING ID</div>
                                        <div className="col-cntnt">{this.props.booking.bookingDetail.id}</div>
                                    </div>
                                    <div className="u-ib u-v-align-middle trip-hdr-block-lg">
                                        <div className="col-hdr">className</div>
                                        <div className="col-cntnt">ECONOMY</div>
                                    </div>
                                    <div className="u-ib u-v-align-middle trip-hdr-block-md">
                                        <div className="col-hdr">BOOKING STATUS</div>
                                        <div className="col-cntnt success">Booking Confirmed</div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-section u-box">
                                <div className="flight-leg-detail">
                                    <TripInfo booking={this.props.booking.bookingDetail} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="u-ib u-layout-col-2">
                        <div className="fare-dtl">
                            <div className="c-accordion-item u-box expanded">
                                <div className="accordion-hdr"><div className="accordion-hdr-cntnt">Payment Detail</div></div>
                                <div className="accordion-body disp">
                                    <div className="fare-dtl-row">
                                        <div className="left-wing">Base Fare</div>
                                        <div className="right-wing">
                                            <div className="c-price-display u-text-ellipsis" title="">
                                                <span className="icon"><i className="ixi-icon-inr icon" aria-hidden="true"></i></span><span className="">{fareDetail.basePrice}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="fare-dtl-row">
                                        <div className="left-wing">Taxes &amp; Fees</div>
                                        <div className="right-wing taxes">
                                            <div className="c-price-display u-text-ellipsis" title="">
                                                <span className="icon"><i className="ixi-icon-inr icon" aria-hidden="true"></i></span><span className="">{fareDetail.tax}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="fare-dtl-row">
                                        <div className="left-wing">Convenience Fee<sup className="sup-link">*</sup></div>
                                        <div className="right-wing">
                                            <div className="c-price-display u-text-ellipsis" title="">
                                                <span className="icon"><i className="ixi-icon-inr icon" aria-hidden="true"></i></span><span className="">{fareDetail.conv}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="fare-dtl-row total">
                                        <div className="left-wing">Amount Paid</div>
                                        <div className="right-wing">
                                            <div className="price u-ib">
                                                <div className="c-price-display u-text-ellipsis" title="">
                                                    <span className="icon"><i className="ixi-icon-inr icon" aria-hidden="true"></i></span><span className="">{fareDetail.totalPrice}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }

    render() {
        return (
            <section className="banner-area relative ser-banner">
                <div className="overlay overlay-bg"></div>
                <div className="container">
                    <div className="row ser-fullscreen align-items-center justify-content-between">
                        <div className="ser-container">
                            <div className="ser-col col-lg-12 col-md-12 banner-left">
                                <h1 className="text-white">Retrieve Booking</h1>
                            </div>
                            <div className="ser-col col-lg-12 col-md-12 banner-left">
                                <div className="tripStatusContainer upcoming">
                                    <form onSubmit={this.onSubmitHandler.bind(this)}>
                                        <div className="tripStatusWrap" id="tripStatusWrap">
                                            <ul className="txtFild">
                                                <li className="txtFild__fieldContanr txtFild__fieldContanr--search">
                                                    <input id="focus" type="text" placeholder="Booking Id" name="id" className="font14 txtFild__inputFld" value={this.state.id}></input>
                                                </li>
                                                <li className="txtFild__fieldContanr txtFild__fieldContanr--search">
                                                    <input id="focus" type="text" placeholder="Passenger Name" name="name" className="font14 txtFild__inputFld" value={this.state.name}></input>
                                                </li>
                                            </ul>
                                            <div className="txtFild-button ">
                                                <div className="txtFild__fieldContanr txtFild__fieldContanr--search">
                                                    <button className="primary-btn text-uppercase">Search Booking</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            {this.state.result ? this.renderBookingDetail() : ""}
                        </div>
                    </div>
                </div>
                <Snackbar open={this.state.formSubmitted} autoHideDuration={this.state.autoHideSnackBarDuration} onClose={(event, reason) => this.handleSnackBarClose(event, reason)}>
                    <Alert onClose={(event, reason) => this.handleSnackBarClose(event, reason)} severity={this.state.formUpdateSeverity}>
                        {this.state.formUpdatedMsg}
                    </Alert>
                </Snackbar>
            </section>
        )
    }
}
Service.propTypes = {
    getBookingDetail: PropTypes.func.isRequired,
    booking: PropTypes.object.isRequired,
    errors: PropTypes.string.isRequired,
    auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    booking: state.booking,
    auth: state.auth,
    errors: state.errors
})

export default connect(mapStateToProps, { getBookingDetail })(Service)
