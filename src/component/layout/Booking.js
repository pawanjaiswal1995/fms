import React, { Component } from 'react'
import "../css/booking.css"
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import data from "../../resource/image.json"
import moment from 'moment';
import TextGroupForm from '../../common/TextGroupForm';
import PopUp from '../../common/PopUp';
import { saveBookingDetail, removeBookingDetail } from '../../actions/bookingActions'
import { removeFlightDetail, removeErrorDetail } from '../../actions/flightActions'

class Booking extends Component {
    constructor(props) {
        super(props);

        this.state = {
            imageData: data,
            pay: false,
            travellerDetail: {},
            paymentDetail: {},
            fareDetail: {},
            flightDetail: {},
            bookingDetail: {},
            popup: false
        };
    }

    componentDidMount() {
        if (Object.keys(this.props.flight.detail).length === 0 ) {
                this.props.history.push('/')
        }
        const fareDetail = {}
        fareDetail["basePrice"] = (this.props.flight.detail.price * this.props.flight.form.adults) + ((this.props.flight.detail.price / 2) * this.props.flight.form.child)
        fareDetail["tax"] = fareDetail["basePrice"] * 0.24
        fareDetail["conv"] = fareDetail["basePrice"] * 0.12
        fareDetail["totalPrice"] = fareDetail["basePrice"] + fareDetail["tax"] + fareDetail["conv"]
        this.setState({ fareDetail: fareDetail, flightDetail: this.props.flight.detail })
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.auth.isAuthenticated) {
            this.props.history.push('/')
        }
    }

    renderAdultTable() {
        const tab = []
        for (let count = 1; count <= parseInt(this.props.flight.form.adults); count++) {
            tab.push(<TextGroupForm key={1} title={"Adult"} pass={this.state.travellerDetail} count={count} />)
        }
        if (this.props.flight.form.child !== "") {
            for (let count = 1; count <= parseInt(this.props.flight.form.child); count++) {
                tab.push(<TextGroupForm key={1 + 10} title={"Child"} pass={this.state.travellerDetail} count={count} />)
            }
        }
        return tab
    }

    submitHandler() {
        this.setState({ pay: true })
    }

    moveHome(e) {
        e.preventDefault()
        this.props.removeErrorDetail("")
        this.props.removeBookingDetail({})
        this.props.removeFlightDetail({})
        this.props.history.push("/")
    }

    revertHandler() {
        this.setState({ pay: false })
    }

    passengerSubmitHandler = (event) => {
        event.preventDefault();
        this.setState({ pay: true })
        const formData = new FormData(event.target);
        const travellerDetail = {};
        for (let [key, value] of formData.entries()) {
            travellerDetail[key] = value
        }
        this.setState({ travellerDetail: travellerDetail })
    }

    paymentSubmitHandler = (event) => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const paymentDetail = {};
        for (let [key, value] of formData.entries()) {
            paymentDetail[key] = value
        }
        const booking = {}
        booking["id"] = Math.floor(1000000 + Math.random() * 9000000);
        booking["bookedDate"] = moment().format("YYYY-MM-DD")
        booking["journeyDate"] = this.props.flight.form.start
        booking["flightDetail"] = this.state.flightDetail
        booking["fareDetail"] = this.state.fareDetail
        booking["travellerDetail"] = this.state.travellerDetail
        booking["paymentDetail"] = paymentDetail
        this.setState({ bookingDetail: booking, popup: true })
        this.props.saveBookingDetail(booking);
    }

    togglePop = () => {
        this.setState({
            popup: !this.state.popup
        });
        this.props.removeErrorDetail("")
        this.props.removeBookingDetail({})
        this.props.removeFlightDetail({})
        this.props.history.push("/")
    };

    render() {
        if (Object.keys(this.props.flight.detail).length === 0) {
            return (
                <div></div>
            )
        }
        else {
            const row = this.props.flight.detail;
            const form = this.props.flight.form;
            var day = moment(form.start).format("DD MMM, ddd");
            var time = moment.utc(moment.duration(row.duration, "minutes").asMilliseconds()).format("HH[hr] mm[m] ")
            return (
                <div className="flight-dt">
                    <div className="page-content u-layout-2-col">
                        <div className="u-ib u-layout-col-1 w5">
                            <div className="trip-dtl">

                                <div className="card-section u-box">
                                    <div className="_3gijNv col-12-12">
                                        <div className="_5eb2GM _2sWRJ2" >
                                            <div className="_3r5AS8">
                                                <div className="_36735z _2MiOSH">1</div>
                                                <div className="_3M-QQB _2trudg">REVIEW ITINERARY</div>
                                            </div>
                                            <div className="_3_hpP1" onClick={this.moveHome.bind(this)}>Change</div>
                                        </div>
                                    </div>
                                    <div className="_3gijNv col-12-12">
                                        <div className="_3pCGqo">
                                            <div className="LfOK6I PoUDAf w1">
                                                {row.originLocation}
                                                <svg xmlns="http://www.w3.org/2000/svg" className="_3jK6iB" width="12" height="12" viewBox="0 0 12 12">
                                                    <g fill="none">
                                                        <path d="M-3-3h18v18H-3z"></path>
                                                        <path fill="#212121" className="" d="M6 0L4.94 1.06l4.19 4.19H0v1.5h9.13l-4.19 4.19L6 12l6-6z"></path>
                                                    </g>
                                                </svg>
                                                {row.destinationLocation}
                                            </div>
                                            <div className="w2">
                                                <img src={this.state.imageData[row.company]} className="flightImagesNew" alt="flights"></img>
                                            </div>
                                            <div className="_1Akcyl w3">
                                                <div className="_2e9p9i">
                                                    <div className="_2hFkF2 _1ehHCj">
                                                        <div className="_3dpPbw">{day}</div>
                                                        <div className="_1zHjqP _1ehHCj _3WzQfI">{row.origin}<span className="_3rXZoA PoUDAf">{row.segment[0].departureTime}</span></div>
                                                    </div>
                                                    <div className="_38Jurv">
                                                        <div className="_1MCcHo">
                                                            <div className="_2VC1PZ">{time}</div>
                                                        </div>
                                                        <div className="nKfxwM">
                                                            <div className="thRaQA"></div>
                                                            <div className="_1ImgDS"></div>
                                                            <div className="_3Vifah"></div>
                                                        </div>
                                                    </div>
                                                    <div className="_2hFkF2 _3tPiVF">
                                                        <div className="_3dpPbw">{day}</div>
                                                        <div className="_1zHjqP _3tPiVF _3WzQfI">{row.destination}<span className="_3RIuo4 PoUDAf">{row.segment.length > 1 ? row.segment[1].arrivalTime : row.segment[0].arrivalTime}</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-section u-box">
                                    <div className="_3gijNv col-12-12">
                                        <div className={`${this.state.pay ? "_5eb2GM _2sWRJ2" : "_5eb2GM _31CvVJ"}`}>
                                            <div className="_3r5AS8">
                                                <div className={`_36735z ${this.state.pay ? "_2MiOSH" : "lqdtEj"}`}>2</div>
                                                <div className={`_3M-QQB ${this.state.pay ? "_2trudg" : "_39FeLb"}`}>TRAVELLER DETAILS</div>
                                            </div>
                                            <div className={`_3_hpP1 ${this.state.pay ? "" : "disable"}`} onClick={this.revertHandler.bind(this)}>Change</div>
                                        </div>
                                    </div>
                                    {this.state.pay ? "" : (
                                        <div className="_3gijNv col-12-12">
                                            <form className="_3ynkj_" onSubmit={this.passengerSubmitHandler}>
                                                {this.renderAdultTable()}
                                                <div className="_34NErh">
                                                    <div className="_1fM-O7">
                                                        <div className="_3lwNK6"><div className="_2aT300">Contact Information</div></div>
                                                        <div className="_2ic7No">Your booking details will be sent here</div>
                                                    </div>
                                                    <div className="row marg">
                                                        <div className="col-4-12 _1SKKEL">
                                                            <div className="_1l_HKs Th26Zc">
                                                                {Object.keys(this.state.travellerDetail).length !== 0 ?
                                                                    (<input type="text" className="_16qL6K _2jcBEn _366U7Q" placeholder="Phone Number" defaultValue={this.state.travellerDetail["Phone Number"]} name="Phone Number" />)
                                                                    :
                                                                    (<input type="text" className="_16qL6K _2jcBEn _366U7Q" placeholder="Phone Number" name="Phone Number" />)}

                                                            </div>
                                                        </div>
                                                        <div className="col-4-12 _1SKKEL">
                                                            <div className="_1l_HKs Th26Zc">
                                                                {Object.keys(this.state.travellerDetail).length !== 0 ?
                                                                    (<input type="text" className="_16qL6K _2jcBEn _366U7Q" placeholder="Email Address" defaultValue={this.state.travellerDetail["Email Address"]} name="Email Address" />)
                                                                    :
                                                                    (<input type="text" className="_16qL6K _2jcBEn _366U7Q" placeholder="Email Address" name="Email Address" />)}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="_10Lps_">
                                                    <div className="_10aYcU _1KxQum">
                                                        <div><p>Your details will be shared with our booking partner to process this booking</p></div>
                                                    </div>
                                                    <button><div className="_1NAnlD">Continue</div></button>
                                                </div>
                                            </form>
                                        </div>)}
                                </div>
                                <div className={`card-section u-box ${this.state.pay ? "" : "disable"}`}>
                                    <div className="_3gijNv col-12-12">
                                        <div className="_5eb2GM _31CvVJ">
                                            <div className="_3r5AS8">
                                                <div className="_36735z lqdtEj">3</div>
                                                <div className="_3M-QQB _39FeLb">Payment</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="_34NErh">
                                        <form className="wBTfBT" onSubmit={this.paymentSubmitHandler}>
                                            <div className="_102Fqh">
                                                <div className="_3J61uL">
                                                    <div className="Th26Zc">
                                                        <input type="text" className="_16qL6K _366U7Q" name="cardNumber" maxLength="16" placeholder="Enter Card Number" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="_3hlxgp">
                                                <span className="_39scaT">Valid thru</span>
                                                <span className="_1ME-Cw">
                                                    <div className="_3tVfLZ">
                                                        <input type="number" className="_1TQIV6 _1E21Zg _8rlsVy" name="month" min="01" max="12"></input>
                                                    </div>
                                                    <div className="_3tVfLZ">
                                                        <input type="number" className="_1TQIV6 _1E21Zg" name="year" min="20" max="35"></input>
                                                    </div>
                                                </span>
                                            </div>
                                            <div className="KaFelO">
                                                <div className="_3x6XEn">
                                                    <div className="Th26Zc">
                                                        <input type="password" className="_16qL6K _366U7Q" maxLength="4" name="cvv" placeholder="CVV" />
                                                    </div>
                                                    <span className="_1Eoqyn">?</span>
                                                </div>
                                            </div>
                                            <div className="_3bRn2V">
                                                <button><div className="_2AkmmA wbv91z _7UHT_c">PAY ₹{this.state.fareDetail.totalPrice}</div></button>
                                            </div>
                                            {this.state.popup ? <PopUp toggle={this.togglePop} bid={this.state.bookingDetail.id} date={this.state.bookingDetail.bookedDate} /> : null}
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="u-ib u-layout-col-2 w6">
                            <div className="_3CKRe3">
                                <div className="_13wOiu">
                                    <div className="_3ymDkb _2YANpz"><span className="_3gLdHT">Fare Details</span></div>
                                    <div className="_2twTWD">
                                        <div className="hJYgKM">
                                            <div className="_10vVqD">Base Fare ({form.totalPasseenger} Travellers)</div>
                                            <span> ₹{this.state.fareDetail.basePrice}</span>
                                        </div>
                                        <div className="hJYgKM">
                                            <div className="_10vVqD">Taxes &amp; Fees</div>
                                            <span> ₹{this.state.fareDetail.tax}</span>
                                        </div>
                                        <div className="hJYgKM">
                                            <div className="_10vVqD">Convenience Fee<span> *</span></div>
                                            <span> ₹{this.state.fareDetail.conv}</span>
                                        </div>
                                        <div className="_3xFQAD">
                                            <div className="hJYgKM">
                                                <div className="_10vVqD">Total Fare ({form.totalPasseenger} Travellers)</div>
                                                <span>
                                                    <div className="tnAu1u">
                                                        <div className="hJYgKM _2UO4l-">
                                                            <div className="_10vVqD"></div>
                                                            <span> ₹{this.state.fareDetail.totalPrice}</span>
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

Booking.propTypes = {
    removeErrorDetail: PropTypes.func.isRequired,
    removeBookingDetail: PropTypes.func.isRequired,
    removeFlightDetail: PropTypes.func.isRequired,
    saveBookingDetail: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.string.isRequired,
    flight: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    flight: state.flight,
    errors: state.errors
})

export default connect(mapStateToProps, { saveBookingDetail, removeFlightDetail, removeBookingDetail, removeErrorDetail })(Booking)