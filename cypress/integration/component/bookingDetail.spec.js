/// <reference types="cypress" />

context('Retrieve Booking Test', () => {
    beforeEach(() => {
        cy.visit('/service')

      })
    it('Search the Booking detail', () => {
        const id = '6241599'
        const name = 'Ajay'

        cy.get("input[placeholder='Booking Id']")
            .type(id)
            .should('have.value',id)

        cy.get("input[placeholder='Passenger Name']")
            .type(name)
            .should('have.value',name)  
        
        cy.get('.primary-btn')
            .click()

        cy.window().its('store').invoke('getState').its('booking.bookingDetail.id').should('eq', 6241599)

        cy.window().its('store').invoke('getState').its('booking.bookingDetail.journeyDate').should('eq', '2020-10-23')

        cy.window().its('store').invoke('getState').its('booking.bookingDetail.travellerDetail').its('First Name 1').should('eq','Ajay')

        cy.window().its('store').invoke('getState').its('booking.bookingDetail.flightDetail.flightNo').should('eq','IND-1245')
        
    })

  })

  