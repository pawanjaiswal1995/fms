/// <reference types="cypress" />

context('Flight Detail Page Filters Test', () => {
    beforeEach(() => {
        const flightDetail = [{"id":"121","flightNo":"IND-1245","company":"IndiGo","exemptDate":["2020-10-25","2020-10-31"],"price":2500,"baggage":"20KG","departureState":"evening","duration":120,"vacancy":19,"Infant":1000,"originLocation":"Delhi,India","destinationLocation":"Bangalore,India","origin":"DEL","destination":"BLR","segment":[{"duration":120,"distance":3000,"departureTime":"18:55","arrivalTime":"20:55","originLocation":"Delhi,India","destinationLocation":"Bangalore,India","originTerminal":"DEL","destinationTerminal":"BLR","originAirport":"Indira Gandhi International Airport ","destinationAirport":"Kempegowda International Airport "}]},{"id":"1211","flightNo":"IND-1245","company":"Spicejet","exemptDate":["2020-10-25","2016-10-31"],"price":5500,"baggage":"20KG","departureState":"noon","duration":120,"vacancy":19,"Infant":1000,"originLocation":"Delhi,India","destinationLocation":"Bangalore,India","origin":"DEL","destination":"BLR","segment":[{"duration":120,"distance":3000,"departureTime":"15:55","arrivalTime":"18:55","originLocation":"Delhi,India","destinationLocation":"Bangalore,India","originTerminal":"DEL","destinationTerminal":"BLR","originAirport":"Indira Gandhi International Airport ","destinationAirport":"Kempegowda International Airport "}]},{"id":"122","flightNo":"AIR-1345","company":"Air Asia","departureState":"noon","exemptDate":["2020-10-29"],"price":7000,"baggage":"20KG","duration":410,"vacancy":20,"Infant":1000,"originLocation":"Delhi,India","destinationLocation":"Bangalore,India","origin":"DEL","destination":"BLR","segment":[{"duration":130,"distance":3000,"departureTime":"13:55","arrivalTime":"15:05","originLocation":"Delhi,India","destinationLocation":"Mumbai,India","originTerminal":"DEL","destinationTerminal":"BOM","connectionDuration":125,"originAirport":"Indira Gandhi International Airport ","destinationAirport":"Chhatrapati Shivaji International Airport"},{"duration":155,"distance":3000,"departureTime":"16:30","arrivalTime":"18:55","originLocation":"Mumbai,India","destinationLocation":"Bangalore,India","originTerminal":"BOM","destinationTerminal":"BLR","originAirport":"Chhatrapati Shivaji International Airport","destinationAirport":"Kempegowda International Airport "}]}]
        const date = {"start":"2020-10-30","adults":"2","child":"","source":"DEL","destination":"BLR","totalPasseenger":"2"}
        localStorage.setItem('flight',JSON.stringify(flightDetail))
        localStorage.setItem('date',JSON.stringify(date))
        cy.visit('/flights')
      })

    it('Test the Stops filter', () => {
        cy.get('[data-cy="stopsFilter1"]')
            .click()
            .wait(2000)

        cy.get('span.grey')
            .click()
            .wait(2000)

        cy.get('[data-cy="stopsFilter0"]')
            .click()
            .wait(2000)

        cy.get('span.grey')
            .click()
            .wait(2000)  
    })

    it('Test the Time filter', () => {
        cy.get('[data-cy="timeFilter1"]')
            .click()
            .wait(2000)

        cy.get('span.grey')
            .click()
            .wait(2000)

        cy.get('[data-cy="timeFilter2"]')
            .click()
            .wait(2000)

        cy.get('span.grey')
            .click()
            .wait(2000)  
    })

  })

  