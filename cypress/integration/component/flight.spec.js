/// <reference types="cypress" />

context('Search Flight Form Test', () => {
    beforeEach(() => {
        cy.visit('/')

      })
    it('Search the flight Search detail', () => {
        const from = 'Delhi,India (DEL)'
        const to = 'Bangalore,India (BLR)'
        const date = '2020-10-30'
        const adult = '2'
        cy.get("input[placeholder='From ']")
            .type(from)
            .should('have.value',from)
            
        cy.get('.react-search-box-dropdown')
            .contains(from)
            .click()

        cy.get("input[placeholder='To ']")
            .type(to)
            .should('have.value',to)

        cy.get('.react-search-box-dropdown')
            .contains(to)
            .click()

        cy.get("input[name='start']")
            .type(date)
            .should('have.value',date)

        cy.get("input[name='adults']")
            .type(adult)
            .should('have.value',adult)
            
        cy.get("input[name='child']")
            .type('0')
            .should('have.value','0')    
        
        cy.get('.oneWay')
            .click()

        cy.window().its('store').invoke('getState').its('flight.detail').should('have.length', 3)

        cy.window().its('store').invoke('getState').its('flight.form.source').should('eq','DEL')

        cy.window().its('store').invoke('getState').its('flight.form.destination').should('eq','BLR')

        cy.url().should('include', '/flight')
        
    })

  })

  