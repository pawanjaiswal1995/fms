/// <reference types="cypress" />

context('Routes Test', () => {
    beforeEach(() => {
        cy.visit('/')
      })
      
    it('Visits the Service Page', () => {
      cy.visit('/service')

      cy.url().should('include', '/service')
    })

    it('Visits the Login Page', () => {
        cy.visit('/login')
  
        cy.url().should('include', '/login')
      })

    it('Visits the Flight Detail Page without login and search', () => {
        cy.visit('/flight')
  
        cy.url().should('include', '/')
      })

    it('Visits the booking Page without login and flight detail', () => {
        cy.visit('/booking')
  
        cy.url().should('include', '/')
      })
  })

  